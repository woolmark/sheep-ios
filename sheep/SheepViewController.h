//
//  SheepViewController.h
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "SheepView.h"

@interface SheepViewController : UIViewController

@property SheepView *sheep;

-(void) loadSheepNumber;

-(void) saveSheepNumber;

@end
