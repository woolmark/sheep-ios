//
//  SheepAppDelegate.h
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SheepViewController;

@interface SheepAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SheepViewController *viewController;

@end
