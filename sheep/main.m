//
//  main.m
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SheepAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SheepAppDelegate class]));
    }
}
