//
//  SheepView.m
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import "SheepView.h"

@implementation SheepView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // self.viewController.view.backgroundColor = [[UIColor alloc] initWithRed:0.471 green:1 blue:0.471 alpha:1];
        self.backgroundColor = [[UIColor alloc] initWithRed:0.588 green:0.588 blue:1 alpha:1];
        
        // load the images
        self.sheep00 = [UIImage imageNamed:@"sheep00.gif"];
        self.sheep01 = [UIImage imageNamed:@"sheep01.gif"];
        self.fence = [UIImage imageNamed:@"background.gif"];

        self.scale = 2;
        self.sheep_add = false;
        self.sheep_number = 0;
        
        self.action = 1;
        
        self.sheep_pos = [[NSMutableArray alloc] init];
        for(int i = 0; i < 100; i++) {
            NSMutableArray *pos = [[NSMutableArray alloc] init];
            [pos addObject:[NSNumber numberWithInt:0]];
            [pos addObject:[NSNumber numberWithInt:-1]];
            [pos addObject:[NSNumber numberWithInt:0]];
            [pos addObject:[NSNumber numberWithInt:0]];
            
            [self.sheep_pos addObject:pos];
        }
        
        self.timer = [NSTimer
                scheduledTimerWithTimeInterval:0.1f
                target:self
                selector:@selector(calc:)
                userInfo:nil
                repeats:YES
              ];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // reset sheep[0] pos
    NSMutableArray *pos = [self.sheep_pos objectAtIndex:0];
    [pos replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:self.frame.size.width]];
    [pos replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:40 * self.scale]];
    [pos replaceObjectAtIndex:3 withObject:[self getJumpXPos:[pos objectAtIndex:1]]];
}

- (void)run {
    if ([self.timer isValid]) {
        [self.timer fire];
    }
}

- (void)stop {
    if (![self.timer isValid]) {
        [self.timer invalidate];
    }
}

-(void)calc:(NSTimer*) timer {
    
    if (self.sheep_add) {
        [self addSheep];
    }
    
    for (int i = 0; i < [self.sheep_pos count]; i++) {
        NSMutableArray *pos = [self.sheep_pos objectAtIndex:i];
        if ([[pos objectAtIndex:1] intValue] < 0) {
            continue;
        }
        
        // move to left
        [pos replaceObjectAtIndex:0 withObject:[NSNumber
            numberWithInt:[[pos objectAtIndex:0] intValue] - 5 * self.scale]];

        // jump
        int xpos = [[pos objectAtIndex:0] intValue];
        int jpos = [[pos objectAtIndex:3] intValue] - self.fence.size.width * self.scale / 10;
        if (xpos > jpos && xpos < jpos + self.sheep00.size.width * self.scale * 1.5f) {
            [pos replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:[[pos objectAtIndex:1] intValue] + 5]];
        } else if (xpos < jpos && xpos > jpos - self.sheep00.size.width * self.scale * 1.5f) {
            [pos replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:[[pos objectAtIndex:1] intValue] - 5]];
            
            if ([[pos objectAtIndex:2] intValue] == 0) {
                self.sheep_number++;
                [pos replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:1]];
            }
        }

        // if sheep goes to away
        if ([[pos objectAtIndex:0] intValue] < self.sheep00.size.width * self.scale * -1) {
            if (i == 0) {
                [pos replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:self.frame.size.width]];
                [pos replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:0]];
            }
            
            else {
                [pos replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:-1]];
            }
        }
    }
    
    
    self.action = 1 - self.action;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // draw ground
    CGRect background = CGRectMake(0, self.frame.size.height - self.fence.size.height * self.scale + 10 * self.scale,
                                   self.frame.size.width, self.fence.size.height * self.scale - 10 * self.scale);
    CGContextSetRGBFillColor(context, 0.471, 1, 0.471, 1);
    CGContextFillRect(context, background);
    
    // draw the fence
    [self.fence drawInRect:CGRectMake(
                                      (self.frame.size.width - self.fence.size.width * self.scale) / 2,
                                      self.frame.size.height - self.fence.size.height * self.scale,
                                      self.fence.size.width * self.scale,
                                      self.fence.size.height * self.scale)];
    
    // draw the sheep
    UIImage *sheep = (self.action == 0 ? self.sheep00 : self.sheep01);
    for (int i = 0; i < [self.sheep_pos count]; i++) {
        NSMutableArray *pos = [self.sheep_pos objectAtIndex:i];
        if ([[pos objectAtIndex:1] intValue] < 0) {
            continue;
        }
        
        [sheep drawInRect:CGRectMake([[pos objectAtIndex:0] intValue],
                                     self.frame.size.height - [[pos objectAtIndex:1] intValue],
                                     self.sheep00.size.width * self.scale,
                                     self.sheep00.size.height * self.scale)];
    }
    
    // draw sheep count
    CGContextSetRGBFillColor(context, 0, 0, 0, 1);
    [[NSString stringWithFormat:@"%d sheep", self.sheep_number]
        drawAtPoint:CGPointMake(5 * self.scale, 5 * self.scale) withFont:[UIFont systemFontOfSize:8.0f * self.scale]];
}

- (void)addSheep {
    
    for (int i = 0; i < [self.sheep_pos count]; i++) {
        NSMutableArray *pos = [self.sheep_pos objectAtIndex:i];
        if ([[pos objectAtIndex:1] intValue] > 0) {
            continue;
        }
        
        NSNumber *y = [NSNumber numberWithInt:arc4random() % (60 * self.scale) + self.sheep00.size.height * self.scale];
//        y = [NSNumber numberWithInt:0 + self.sheep00.size.height * self.scale];
        [pos replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:self.frame.size.width]];
        [pos replaceObjectAtIndex:1 withObject:y];
        [pos replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:0]];
        [pos replaceObjectAtIndex:3 withObject:[self getJumpXPos:y]];
        break;
    }
}

- (NSNumber *)getJumpXPos: (NSNumber *)y {
    return [NSNumber numberWithInt:((self.frame.size.width) / 2 -
                                    self.fence.size.width * self.scale / 2 +
                                    3 * ([y intValue] - self.sheep00.size.height * self.scale) / 4)];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.sheep_add = true;
}

- (void)touchesEnded: (NSSet *)touches withEvent:(UIEvent *)event {
    self.sheep_add = false;
}

@end
