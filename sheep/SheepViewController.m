//
//  SheepViewController.m
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import "SheepViewController.h"

@implementation SheepViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sheep = [[SheepView alloc] init];
    self.sheep.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view = self.sheep;
    
    [self setTitle:@("sheep")];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadSheepNumber];
    [self.sheep run];
}

- (void)viewDidUnload
{
}

-(void) loadSheepNumber
{
    if ([self isViewLoaded]) {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        self.sheep.sheep_number = [ud integerForKey:@"number"];
    }
}

-(void) saveSheepNumber
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setInteger:self.sheep.sheep_number forKey:@"number"];
    [ud synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
