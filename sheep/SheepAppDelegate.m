//
//  SheepAppDelegate.m
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import "SheepAppDelegate.h"
#import "SheepViewController.h"

@implementation SheepAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[SheepViewController alloc] init];
    } else {
        self.viewController = [[SheepViewController alloc] init];
        //self.viewController = [[SheepViewController alloc] initWithNibName:@"SheepViewController_iPad" bundle:nil];
    }
    
    //self.window.rootViewController = self.viewController;
    
    UINavigationController *naviController = [[UINavigationController alloc]
                                              initWithRootViewController:(self.viewController)];
    [naviController setNavigationBarHidden:NO animated:NO];
    
    [self.window makeKeyAndVisible];
    [self.window addSubview:naviController.view];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self.viewController saveSheepNumber];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self.viewController loadSheepNumber];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
